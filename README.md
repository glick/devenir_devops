# Devenir DevOps / DevOps Roadmap

## Sources

Plan de formation pour devenir DevOps, du serveur au "sans serveur"  
principalement inspiré de:  
- [The Roadmap to Becoming a DevOps Dude — From Server to Serverless](https://hackernoon.com/the-roadmap-to-become-a-devops-dude-from-server-to-serverless-dd97420f640e)  
- [DevOps Roadmap - Guide to becoming an SRE or for any other operations role](https://roadmap.sh/devops)
- Test: [Awesome Quality Assurance Roadmap](https://github.com/fityanos/awesome-quality-assurance-roadmap)

- A detailed checklist: [The Must Know Checklist For DevOps & Site Reliability Engineers](https://hackernoon.com/the-must-know-checklist-for-devops-site-reliability-engineers-update-8ba44dbc824)

- [The Future of DevOps: 15 Trends for 2021](https://www.faun.dev/c/stories/eon01/the-future-of-devops-15-trends-for-2021/)

[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/glick/devenir_devops/master?grs=gitlab)


## Légende

```mermaid
graph TD
subgraph "Légende"
  Step1(Etape 1)
  Step1 --> Step2(Etape 2)
  Step1 -.-> Recommendation["Recommandation #9829;"]
  Step1 -.-> Option
end

classDef Reco fill:#ffff00;
class Recommendation Reco;
```  

## Commun

```mermaid
graph TB
subgraph "Prérequis communs aux développeur frontend, développeur backend, devops ..."
  Culture(Culture) --> SCM
  Culture -.-> CALMS(Les 5 piliers CALMS)
  Culture -.-> Manifesto("Le Manifeste Agile (4 valeurs prioritaires, 12 principes)")
  click Manifesto "https://agilemanifesto.org/" "agile manifesto"
  Culture -.-> SaaS(SaaS: 12 facteurs)
  click SaaS "https://12factor.net/" "The twelve-factor app"
  Culture -.-> CheckList(La liste devops en 15 pts)
  click CheckList "https://medium.com/faun/the-15-point-devops-check-list-8cd2afb4a448" "The 15-point DevOps Checklist"

  SCM(Gestion de Version)
  SCM --> SemVer(Gestion Sémantique de Version)
  click SemVer "https://semver.org/" "Semantic Versioning"
  SCM -.-> git

  SemVer --> Basic_Terminal(Utilisation basique d'un Terminal)
  Basic_Terminal --> Algo(Structures de données et Algorithmes)
  Algo --> Principles(SOLID, KISS, YAGNI, DRY, ...)
  Principles --> Forge(Forge)
  Forge --> Licences(Licences)
  Forge -.-> GitLab
  Forge -.-> GitHub
  Forge -.-> Atlassian["Outils Atlassian (Jira, Confluence, Bamboo, Bitbucket, Trello)"]

  Licences --> SSH(SSH)
  SSH --> HTTP(HTTP HTTPS API)
  HTTP --> Patterns("Modèles de Conception (Design Patterns)")
  Patterns --> Encoding(Encodage des caractères)
end

classDef Reco fill:#ffff00;
class git,GitLab Reco;
```

## DevOps

```mermaid
graph TB

subgraph "DevOps"
  Prerequisites(0 Prérequis) --> Language(1 Apprendre un Langage de programmation)
  Language --> OS_Concepts(2 Comprendre différents concepts d'OS)
  Language -.-> Python
  Language -.-> Ruby
  Language -.-> Node.js

  OS_Concepts --> Servers(3 Apprendre à gérer des Serveurs)
  Servers --> Network(4 Réseau et Sécurité)
  Servers --> OS(OS)
  OS -.-> Linux
  OS -.-> Unix
  OS -.-> Windows
  Servers --> Terminal(Apprendre à vivre dans un Terminal)

  Network --> WhatConf(5 Qu'est-ce que et comment configurer un ... ?)
  WhatConf --> Infra(6 Infrastructure as Code)
  WhatConf -.-> Webserver(Serveur Web)
  Webserver -.-> Apache
  Webserver -.-> Nginx
  Webserver -.-> IIS

  Infra --> CICD(7 CI/CD)
  Infra -.-> Docker
  Infra -.-> Ansible
  Infra -.-> Kubernetes
  Infra -.-> Terraform

  CICD --> Monitoring(8 Monitoring)
  CICD -.-> GitLabCI[GitLab CI]
  CICD -.-> Jenkins
  CICD -.-> tests[Tous les types de tests]

  Monitoring --> Clouds(9 Fournisseurs de Clouds)
  Monitoring -.-> ELK
  Monitoring -.-> Grafana
  Monitoring -.-> Prometheus
  Monitoring -.-> Loki

  Clouds --> Serverless(10 Serverless)
  Clouds -.-> AWS
  Clouds -.-> GCP[Google Cloud]
  Clouds -.-> Azure
  Clouds -.-> OpenStack
  Clouds -.-> CNCF[CNCF Landscape]
  click CNCF "https://landscape.cncf.io/" "CNCF Cloud Native Interactive Landscape"

  CNCF -.-> TrailMap[CNCF Trail Map]
  click TrailMap "https://raw.githubusercontent.com/cncf/trailmap/master/CNCF_TrailMap_latest.png" "Cloud Native Trail Map"

  Serverless --> xOps(11 .*Ops)
  Serverless -.-> Lambda[AWS Lambda]
  Serverless -.-> AF[Azure Functions]
  Serverless -.-> GCF[Google Cloud Functions]

  xOps -.-> DevSecOps
  xOps -.-> GitOps[GitOps IaC]
  xOps -.-> ChatOps
  xOps -.-> Chaos
  xOps -.-> AIOps[AI ML Data Ops]

end

classDef Reco fill:#ffff00;
class Python,Linux,Apache,Nginx,GitLabCI,Docker,Ansible,Kubernetes,AWS,DevSecOps,tests Reco;

```
