
# Devenir DevOps

Plan de formation pour devenir DevOps

Gaël Lickindorf - Consultant DevOps NeoSoft

Juillet 2019

+++

## Introduction

Présentation en ligne

https://gitlab.com/-/ide/project/glick/devenir_devops  
https://gitpitch.com/glick/devenir_devops/master?grs=gitlab  

+++

## Sommaire

1. Commencer par apprendre la culture
1. Apprendre un langage de programmation
1. Comment gérer les serveurs
1. Les bases du réseau et de la sécurité
1. Script
1. Comment installer et configurer des middlewares
1. Comment déployer un logiciel
1. git

+++

## Sommaire (suite)

<p align="left">
9\. Comment construire un logiciel  
10\. Comment automatiser votre usine logicielle  
11\. Gestion de Configuration  
12\. Infrastructure As Code  
13\. Comment monitorer le logiciel et l'Infrastructure  
14\. Containers & Orchestration  
15\. Comment déployer et gérer des Applications Serverless  
16\. Apprendre et partager  
</p>

Note:
- sofware factory, pipeline: GitLab CI, Jenkins
- conf et orchestration: Saltstack , Ansible, Chef, Puppet


---
## 1. Commencer par apprendre la culture

DevOps est un mouvement et une culture avant d'être un métier.  

- 5 piliers: CALMS

- Le manifeste Agile: 4 valeurs 12 principes ...

- SaaS : 12 Facteurs

- La checklist de la culture devops en 15 points

+++
### Culture: 5 piliers: CALMS

Culture, Automatisation, Lean, Métriques, Solidarité

+++
### Culture Agile : le manifeste

DevOps s'inspire d'Agile:  
« Les individus et les interactions plutôt que les processus et les outils »  
https://agilemanifesto.org

+++
### Culture Agile : autres principes

- pas commité, ça n'existe pas
- pas testé, c'est cassé
- le droit à l'erreur

+++
### Culture: SaaS : 12 Facteurs

Software as a Service
https://12factor.net/

+++
### La culture devops en 15 points

- Une équipe pluridisciplinaire
- La culture de la com° et la pensée globale
- Une culture axée sur le client
- Gestion des sources et révisions
- Infrastructure as code
- Automatisation de routine
- Configuration en libre-service
- Constructions automatisées

+++
### La culture en 15 points (suite)

- Intégration continue CI
- Livraison continue CD
- Tests incrémentaux et développement piloté par les tests TDD
- Gestion automatisée des livraisons
- Cycles de développement et temps de mise sur le marché TTM plus courts
- Indicateurs de performance clés KPI
- La boucle de rétroaction (Feedback)

+++
### Résumé culturel

@css[fragment](Test First, Fail Fast,) @css[fragment](Learn Faster, Success Faster)

@css[fragment](Tester d'abord, échouer rapidement,)  
@css[fragment](apprendre plus vite, réussir plus vite)

COMMENCER PAR TESTER (TDD, CI/CD) <!-- .element: class="fragment" --> 


---
## Sources

- [The Roadmap to Becoming a DevOps Dude — From Server to Serverless](https://hackernoon.com/the-roadmap-to-become-a-devops-dude-from-server-to-serverless-dd97420f640e)
- [The 15-point DevOps Check List](https://medium.com/faun/the-15-point-devops-check-list-8cd2afb4a448)